package com.qiangzi.workflow.engine.bpmn.process;

import org.dom4j.Element;

import com.qiangzi.workflow.engine.bpmn.base.Node;

public abstract class Process extends Node {

	public void parse(Element element) throws Exception {
		super.parse(element);
	}

	public void deploy() {
		super.deploy();
	}

}
