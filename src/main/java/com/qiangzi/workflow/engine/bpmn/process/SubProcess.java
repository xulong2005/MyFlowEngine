package com.qiangzi.workflow.engine.bpmn.process;

import org.dom4j.Element;
import org.springframework.util.Assert;

import com.qiangzi.workflow.engine.bpmn.base.ProcessDefinition;
import com.qiangzi.workflow.engine.bpmn.base.ProcessInstance;
import com.qiangzi.workflow.engine.core.MyEngine;

public class SubProcess extends Process {

	public void parse(Element element) throws Exception {

		super.parse(element);
		// 确保没有name属性被设置
		Assert.isTrue(null == super.getName(), "name attribute not allowed in subProcess node");

	}

	public void deploy() {
		super.deploy();
	}

	/**
	 * 覆盖
	 * 
	 * @throws Exception
	 */
	@Override
	public void invoke(ProcessInstance parentInstance, MyEngine engine) throws Exception {

		// 执行当前子流程+找到这个子流程的定义
		String subProcessId = super.getId();
		ProcessDefinition subProcessDefinition = engine.getParsedProcessDefinitions().get(subProcessId);
		Assert.isTrue(null != subProcessDefinition, "ProcessDefinition not found for " + subProcessId);
		// 生成实例+传递参数上下文
		ProcessInstance subProcessInstance = engine.deploy(subProcessDefinition);
		subProcessInstance.setJuelContext(parentInstance.getJuelContext());
		subProcessInstance.setJuelExpressionFactory(parentInstance.getJuelExpressionFactory());
		// 然后要运行这个子流程
		engine.run(subProcessInstance);
		// 然后直接复用了父类的逻辑
		super.invoke(parentInstance, engine);

	}

}