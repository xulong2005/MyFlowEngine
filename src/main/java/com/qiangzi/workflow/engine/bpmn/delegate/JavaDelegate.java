package com.qiangzi.workflow.engine.bpmn.delegate;

public interface JavaDelegate {

	void execute(DelegateExecution execution) throws Exception;

}
