package com.qiangzi.workflow.engine.bpmn.task;

import org.dom4j.Element;
import com.qiangzi.workflow.engine.bpmn.base.Node;

public abstract class Task extends Node {

	public void parse(Element element) throws Exception {
		super.parse(element);
	}

	public void deploy() {
		super.deploy();
	}

}