package com.qiangzi.workflow.engine.bpmn.flow;

import org.dom4j.Element;

import com.qiangzi.workflow.engine.bpmn.base.Edge;

public abstract class Flow extends Edge {

	public void parse(Element element) throws Exception {
		super.parse(element);
	}

	protected void deploy() {
		super.deploy();
	}

}