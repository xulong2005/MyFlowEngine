package com.qiangzi.workflow.engine.service;

import com.qiangzi.workflow.engine.bpmn.base.ProcessInstance;
import com.qiangzi.workflow.engine.core.MyEngine;

public interface RuntimeService {

	public void run(ProcessInstance instance, MyEngine engine) throws Exception;

}
